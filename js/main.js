// "hello_text"idのテキスト変更
document.getElementById("hello_text").textContent = "はじめてのJavaScript";


//回数カウントの変数定義
var count = 0;

// ゲーム盤を示す変数
var cells;

// 落下中フラグ
var isFalling = false;

// 落下中ブロック番号
var fallingBlockNum = 0;

// ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};

// ゲーム盤を読み込む
loadTable();

// 一定時間で繰り返す
setInterval(function() {
    // 何回目かを数えるために変数countを1ずつ増やす
    count++;
    document.getElementById("hello_text").textContent = "First JavaScript - (" + count + ")";

    // 落下中のブロックがあるか確認
    if (hasFallingBlock()) {
        // ブロックを落とす
        fallBlocks();
    } else {
        // ランダムにブロックを作成
        generateBlock();
        //ブロックが積み上がり切っていないかチェック
        // for (var row = 0; row < 1; row++) {
        //     for (var col = 0; col < 10; col++) {
        //         if (cells[row][col].className !== "") {
        //             alert("Game Over");
        //         }
        //     }
        // }
        // 揃っている行を削除
        deleteRow();
    }
}, 300);

/* ------ ここから下は関数の宣言部分 ------ */

function loadTable() {
    // タグの名前から表の要素取得
    var td_array = document.getElementsByTagName("td");

    cells = [];
    var index = 0;

    // 配列要素をさらに配列にする（2次元配列にする）
    for (var row = 0; row < 20; row++) {
        cells[row] = [];
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }

}

//ブロックを落とす処理
function fallBlocks() {
    // 1. 底についていないか？
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {
            // 一番下の行にブロックがいるので落とさない
            isFalling = false;
            return;
        }
    }
    // 2. // 下の列に別のブロックがないかチェック
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return; // 一つ下のマスにブロックがいるので落とさない
                }
            }
        }
    }
    // 下から二番目の行から繰り返しクラスを下げていく
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                // 上の行を、下の行に移行する
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                // 上の行を空にする
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}


//そろっている行を消す処理
function deleteRow() {
    for (var row = 19; row >= 0; row--) {
        var canDelete = true;
        for (var col = 0; col < 10; col++) {
            // 削除可能か判別
            if (cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if (canDelete) {
            //1行消す
            for (var col = 0; col < 10; col++) {
                //console.log(row, col)
                cells[row][col].className = "";
            }
            //削除したi一行の分、上の行のブロックをすべて1マス落とす
            for (var downRow = row - 1; downRow >= 0; downRow--) {
                for (var col = 0; col < 10; col++) {
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}


// ランダムにブロックを生成する処理
function generateBlock() {
    // 1. ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 2. 選んだパターンをもとにブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3. 落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

function hasFallingBlock() {
    // 落下中のブロックがあるか確認する
    return isFalling;
}


// キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
    if (event.keyCode === 37) {
        moveLeft();
    } else if (event.keyCode === 39) {
        moveRight();
    }
}


//ブロックを右に移動させる処理
function moveRight() {
    //右端についてないか
    for (var i = 0; i < 20; i++) {
        if (cells[i][9].blockNum === fallingBlockNum) {
            return;
        }
    }

    //１マス右にブロックがないか
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum) {
                    return;
                }
            }
        }
    }

    // ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

//ブロックを左に移動させる処理
function moveLeft() {
    //左端についてないか
    for (var i = 0; i < 20; i++) {
        if (cells[i][0].blockNum === fallingBlockNum) {
            return;
        }
    }

    //左にブロックがないか
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 1; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col - 1].className !== "" && cells[row][col - 1].blockNum !== fallingBlockNum) {
                    return;
                }
            }
        }
    }

    // ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}